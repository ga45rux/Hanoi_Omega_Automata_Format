(*
  Author:   Benedikt Seidl
  License:  GPLv3
*)

section \<open>Example automaton in the HOA format\<close>

theory HOA_Automaton_Example
  imports Main HOA_Automaton
begin

  definition "\<Sigma> \<equiv> [''0'', ''1'']"
  definition "\<delta> \<equiv> [{(AP\<^sub>L 0, 0, {}), (AP\<^sub>L 1, 0, {}), (AP\<^sub>L 0, 1, {})}, {(AP\<^sub>L 0, 1, {0})}]"
  definition "Q\<^sub>0 \<equiv> {0}"
  definition "\<alpha> \<equiv> Inf\<^sub>A 0"
  definition "A :: string hoa_automaton \<equiv> (\<Sigma>, \<delta>, Q\<^sub>0, \<alpha>)"

  lemma[simp]: "hoa_automaton A"
    by (simp add: hoa_automaton_def A_def \<Sigma>_def \<delta>_def Q\<^sub>0_def \<alpha>_def)

  lemma w'_w: "hoa_automaton.w' A w i = {0} \<longleftrightarrow> w i = {''0''}" (is "?lhs \<longleftrightarrow> ?rhs")
    proof
      assume ?lhs

      then have "index \<Sigma> ` (w i) = {0}" using hoa_automaton.w'_def sorry

      then have "index \<Sigma> ` (w i) = {index \<Sigma> ''0''}" sorry

      then have "index \<Sigma> ` (w i) = index \<Sigma> ` {''0''}" by simp

      then show ?rhs using inj_on_index sorry
    next
      assume ?rhs

      then have "index \<Sigma> ` (w i) = {index \<Sigma> ''0''}"
        by auto

      then have "index \<Sigma> ` (w i) = {0}" using hoa_automaton.w'_def sorry

      show ?lhs sorry
    qed

  lemma acceptance: "accept\<^sub>H A w \<longleftrightarrow> (\<exists>n. \<forall>i > n. w i = {''0''})" (is "?lhs \<longleftrightarrow> ?rhs")
    proof
      assume ?lhs

      then obtain r where is_run: "hoa_automaton.is_run A w r"
          and accepting: "accepting \<alpha> (inf_marks r)"
        by (auto simp: hoa_automaton.accept_def)

      from accepting have "0 \<in> \<Union> limit (marks o r)" unfolding \<alpha>_def inf_marks_def by simp
      then have "\<exists>x. 0 \<in> x \<and> x \<in> limit (marks o r)" by blast
      then obtain x where "0 \<in> x \<and> x \<in> limit (marks o r)" by blast
      then have "True" using limit_o_inv sorry

      (***)

      have "{0} = \<Union> limit (marks o r)" sorry

      then obtain n where "\<forall>i > n. r i = (1, {1}, 1, {0})" sorry

      then have "\<forall>i > n. hoa_automaton.w' A w i = {0}" using is_run sorry

      then have "\<forall>i > n. w i = {''0''}" using w'_w by blast

      then show ?rhs by blast
    next
      assume ?rhs

      then obtain n where "\<forall>i > n. w i = {''0''}" by blast

      then have "\<forall>i > n. hoa_automaton.w' A w i = {0}" using w'_w by blast

      let ?r = undefined

      have is_run: "hoa_automaton.is_run A w ?r" sorry

      have accepting: "accepting \<alpha> (inf_marks ?r)" sorry

      from is_run accepting show ?lhs using hoa_automaton.accept_def
        by (auto simp: hoa_automaton.accept_def)
    qed

  
  lemma
      defines w: "w i \<equiv> {''0''}"
      shows "accept\<^sub>H A w"
    proof -
      have "\<forall>i. w i = {''0''}" using w
        by simp
      thus ?thesis using acceptance
        by auto
    qed
  
  lemma
      defines w: "w i \<equiv> if i \<le> 5 then {''1''} else {''0''}"
      shows "accept\<^sub>H A w"
    proof -
      have "\<forall>i > 5. w i = {''0''}" using w
        by simp
      thus ?thesis using acceptance
        by auto
    qed

  lemma
      defines w: "w i \<equiv> {''1''}"
      shows "\<not> accept\<^sub>H A w"
    proof -
      have "\<forall>i. w i = {''1''}" using w
        by simp
      thus ?thesis using acceptance
        by auto
    qed

  lemma
      defines w: "w i \<equiv> if even i then {''0''} else {''1''}"
      shows "\<not> accept\<^sub>H A w"
    proof -
      have "\<forall>n. \<exists>i > n. w i = {''1''}" (is "\<forall>n. ?P n")
      proof
        fix n show "?P n"
        proof (cases "even n")
          case True
          hence "w (n + 1) = {''1''}"
            using w by auto
          thus "\<exists>i > n. w i = {''1''}"
            by auto
        next
          case False
          hence "n + 2 > n \<and> w (n + 2) = {''1''}"
            using w by auto
          thus "\<exists>i > n. w i = {''1''}" (is "\<exists>i. ?Q i")
            by (rule exI[of ?Q "(n + 2)"])
        qed
      qed

      thus ?thesis using acceptance
        by fastforce
    qed

end