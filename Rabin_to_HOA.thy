(*
  Author:   Benedikt Seidl
  License:  GPLv3
*)

section \<open>Conversion of Rabin automata to the HOA representation\<close>

theory Rabin_to_HOA
  imports Main HOA_Automaton "LTL_to_DRA.DTS" "LTL_to_DRA.Rabin" "HOL.Finite_Set" "List-Index.List_Index"
begin

type_synonym ('q, 'a) hoa_acceptance = "('q, 'a) transition set acceptance"

definition rabin_condition :: "('q, 'a set) rabin_condition \<Rightarrow> ('q, 'a set) transition set \<Rightarrow> bool" where
  "rabin_condition \<alpha> ts \<equiv> \<exists>(Fin, Inf) \<in> \<alpha>. Fin \<inter> ts = {} \<and> Inf \<inter> ts \<noteq> {}"

lemma "accept\<^sub>R (\<delta>, q\<^sub>0, \<alpha>) w = rabin_condition \<alpha> (limit (run\<^sub>t \<delta> q\<^sub>0 w))"
  unfolding accept\<^sub>R_def rabin_condition_def by fastforce

locale rabin_to_hoa_conversion =
  fixes
    --\<open>Fix types for locale extension\<close>
    types :: "'q \<times> 'a"
  fixes
    --\<open>Mark assigned to a transition set\<close>
    mark :: "('q, 'a set) transition set \<Rightarrow> mark"
  fixes
    --\<open>Label for a set of propositions\<close>
    label :: "'a list \<Rightarrow> 'a set \<Rightarrow> 'a label"
  fixes
    --\<open>Acceptance expression based on rabin condition\<close>
    acceptance :: "('q, 'a set) rabin_condition \<Rightarrow> ('q, 'a set) hoa_acceptance"
  assumes
    --\<open>Each mark is only used for one transition set\<close>
    mark: "inj mark"
  assumes
    --\<open>Labels match the deterministic transitions\<close>
    label: "eval (label \<Sigma> a1) a2 \<longleftrightarrow> a1 \<inter> set \<Sigma> = a2 \<inter> set \<Sigma>"
  assumes
    --\<open>Acceptance matches the defined rabin condition\<close>
    acceptance: "accepting (acceptance \<alpha>) (Pow ts) \<longleftrightarrow> rabin_condition \<alpha> ts"
begin

definition marks :: "('q, 'a set) transition \<Rightarrow> mark set" where
  "marks t \<equiv> {mark s | s. t \<in> s}"

end

locale rabin_to_hoa =
  --\<open>Extend locale and fix types to linorder\<close>
  rabin_to_hoa_conversion types for types :: "'q::linorder \<times> 'a::linorder" +
  fixes
    --\<open>Rabin automaton\<close>
    rabin :: "('q, 'a set) rabin_automaton"
  fixes
    --\<open>Alphabet\<close>
    \<Sigma> :: "'a set"
  fixes
    --\<open>Transition system\<close>
    \<delta> :: "('q, 'a set)  DTS"
  fixes
    --\<open>Initial state\<close>
    q\<^sub>0 :: 'q
  fixes
    --\<open>Acceptance condition\<close>
    \<alpha> :: "('q, 'a set) rabin_condition"
  defines
    "\<delta> \<equiv> fst rabin"
  defines
    "q\<^sub>0 \<equiv> fst (snd rabin)"
  defines
    "\<alpha> \<equiv> snd (snd rabin)"
begin

subsection \<open>Conversion\<close>

--\<open>Atomic propositions\<close>

definition "aps \<equiv> sorted_list_of_set \<Sigma>"
definition "ap\<^sub>H \<equiv> index aps"
definition "ap\<^sub>R \<equiv> nth aps"

lemma ap\<^sub>H_ap\<^sub>R_id[simp]: "a < length aps \<Longrightarrow> ap\<^sub>H (ap\<^sub>R a) = a"
  unfolding ap\<^sub>H_def ap\<^sub>R_def aps_def
  using index_nth_id distinct_sorted_list_of_set
  by blast

lemma ap\<^sub>R_ap\<^sub>H_id[simp]: "a \<in> set aps \<Longrightarrow> ap\<^sub>R (ap\<^sub>H a) = a"
  unfolding ap\<^sub>H_def ap\<^sub>R_def aps_def
  using nth_index
  by simp

definition is_ap\<^sub>H_set :: "ap set \<Rightarrow> bool" where
  "is_ap\<^sub>H_set A \<equiv> \<forall>ap \<in> A. ap < length aps"

definition is_ap\<^sub>R_set :: "'a set \<Rightarrow> bool" where
  "is_ap\<^sub>R_set A \<equiv> \<forall>ap \<in> A. ap \<in> set aps"

lemma ap\<^sub>H_ap\<^sub>R_image[simp]: "is_ap\<^sub>H_set A \<Longrightarrow> ap\<^sub>H ` ap\<^sub>R ` A = A"
  unfolding is_ap\<^sub>H_set_def
  by (auto simp: image_image image_iff)

lemma ap\<^sub>R_ap\<^sub>H_image[simp]: "is_ap\<^sub>R_set A \<Longrightarrow> ap\<^sub>R ` ap\<^sub>H ` A = A"
  unfolding is_ap\<^sub>R_set_def
  by (auto simp: image_image image_iff)

--\<open>States\<close>

definition reach :: "'q set" where
  "reach \<equiv> DTS.reach (Pow \<Sigma>) \<delta> q\<^sub>0"

definition "states \<equiv> sorted_list_of_set reach"
definition "state\<^sub>H \<equiv> index states"
definition "state\<^sub>R \<equiv> nth states"

lemma state\<^sub>H_state\<^sub>R_id[simp]: "q < length states \<Longrightarrow> state\<^sub>H (state\<^sub>R q) = q"
  unfolding state\<^sub>H_def state\<^sub>R_def states_def
  using index_nth_id distinct_sorted_list_of_set
  by blast

lemma state\<^sub>R_state\<^sub>H_id[simp]: "q \<in> set states \<Longrightarrow> state\<^sub>R (state\<^sub>H q) = q"
  unfolding state\<^sub>H_def state\<^sub>R_def states_def
  using nth_index
  by simp

--\<open>Labels\<close>

definition "label\<^sub>H \<equiv> map_label ap\<^sub>H o label aps"

--\<open>Transitions\<close>

definition reach\<^sub>t :: "'q \<Rightarrow> ('q, 'a set) transition set" where
  "reach\<^sub>t \<equiv> DTS.reach\<^sub>t (Pow \<Sigma>) \<delta>"

definition transition\<^sub>H :: "('q, 'a set) transition \<Rightarrow> transition\<^sub>H" where
  "transition\<^sub>H t \<equiv> let (q, a, p) = t in (label\<^sub>H a, state\<^sub>H q, marks t)"

definition trans :: "transition\<^sub>H set list" where
  "trans \<equiv> map (\<lambda>q. transition\<^sub>H ` reach\<^sub>t q) states"

--\<open>Acceptance\<close>

definition "acceptance\<^sub>H \<equiv> map_acceptance mark o acceptance"

--\<open>Automaton\<close>

definition hoa :: "'a hoa_automaton" where
  "hoa \<equiv> (aps, trans, {state\<^sub>H q\<^sub>0}, acceptance\<^sub>H \<alpha>)"

--\<open>Runs\<close>

definition is_hoa_run :: "run\<^sub>H \<Rightarrow> bool" where
  "is_hoa_run r \<equiv> \<forall>i.
    fst (r i) < length states \<and>
    is_ap\<^sub>H_set (fst (snd (r i))) \<and>
    fst (snd (snd (r i))) < length states"

(* TODO add condition for marks *)

definition is_rabin_run :: "('q, 'a set) transition word \<Rightarrow> bool" where
  "is_rabin_run r \<equiv> \<forall>i.
    fst (r i) \<in> set states \<and>
    is_ap\<^sub>R_set (fst (snd (r i))) \<and>
    snd (snd (r i)) \<in> set states"


definition hoa_to_rabin_run :: "run\<^sub>H \<Rightarrow> ('q, 'a set) transition word" where
  "hoa_to_rabin_run r i \<equiv>
    (state\<^sub>R (fst (r i)), ap\<^sub>R ` (fst (snd (r i))), state\<^sub>R (fst (snd (snd (r i)))))"

definition rabin_to_hoa_run :: "('q, 'a set) transition word \<Rightarrow> run\<^sub>H" where
  "rabin_to_hoa_run r i \<equiv>
    (state\<^sub>H (fst (r i)), ap\<^sub>H ` (fst (snd (r i))), state\<^sub>H (snd (snd (r i))), marks (r i))"


lemma "is_rabin_run r \<Longrightarrow> hoa_to_rabin_run (rabin_to_hoa_run r) = r"
  unfolding is_rabin_run_def hoa_to_rabin_run_def rabin_to_hoa_run_def
  by auto

lemma "is_hoa_run r \<Longrightarrow> rabin_to_hoa_run (hoa_to_rabin_run r) = r"
  unfolding is_hoa_run_def rabin_to_hoa_run_def hoa_to_rabin_run_def
  apply (auto)
  oops

(* missing predicate for marks *)

lemma
  fixes r\<^sub>H :: "run\<^sub>H"
  defines "r\<^sub>R \<equiv> hoa_to_rabin_run r\<^sub>H"
  shows "\<Union> (marks\<^sub>R ` limit r\<^sub>R) = inf_marks r\<^sub>H"
sorry


subsection \<open>Correctness\<close>

  theorem inv: "hoa_automaton hoa"
    proof (unfold hoa_automaton_def; safe)
      fix t q a p
      assume "t \<in> set (trans\<^sub>A hoa)" and "(q, a, p) \<in> t"

      then show "max_ap q < length (aps\<^sub>A hoa)" sorry
    next
      fix q
      assume "q \<in> init\<^sub>A hoa"

      then show "q < length (trans\<^sub>A hoa)" sorry
    next
      fix t q a p
      assume "t \<in> set (trans\<^sub>A hoa)" and "(q, a, p) \<in> t"

      then show "a < length (trans\<^sub>A hoa)" sorry
    qed

  theorem equiv: "accept\<^sub>H hoa w = accept\<^sub>R rabin w"
    proof
      assume "accept\<^sub>H hoa w"
      then obtain r where
          run: "hoa_automaton.is_run hoa w r" and
          acc: "accepting (acceptance\<^sub>H \<alpha>) (inf_marks r)"
        unfolding accept\<^sub>H_def using hoa_def inv by auto

      then show "accept\<^sub>R rabin w" sorry
    next
      assume "accept\<^sub>R rabin w"
      then obtain a b where
          pair: "(a, b) \<in> \<alpha>" and
          acc: "accepting_pair\<^sub>R \<delta> q\<^sub>0 (a, b) w"
        unfolding accept\<^sub>R_def \<delta>_def q\<^sub>0_def \<alpha>_def by fast


      show "accept\<^sub>H hoa w" sorry
    oops

end

abbreviation "rabin2hoa \<equiv> rabin_to_hoa.hoa"

end